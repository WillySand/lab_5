from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path('make', views.make),
    path('', views.jadwal, name='jadwal'),
    path('remove', views.remove)
]
