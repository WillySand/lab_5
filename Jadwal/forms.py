from . import models
from django import forms
from datetime import datetime


class JadwalForm(forms.ModelForm):
        event = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "charfield",
                "required" : True,
                "placeholder":"Event",
                }))
        location = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "charfield",
                "required" : True,
                "placeholder":"Location",
                }))
        category = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "charfield",
                "required" : True,
                "placeholder":"Category",
                }))
        date = forms.DateField(widget=forms.SelectDateWidget(attrs={
                "class" : "datefield",
                "required" : True,
                }), initial=datetime.now() )
                
        time = forms.TimeField(widget=forms.TimeInput(attrs={
                "class" : "time",
                "required" : True,
                }), initial=datetime.now() )
        class Meta:
                model = models.Jadwal
                
                fields = ["event", "location", "category", "date", "time"]