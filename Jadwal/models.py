from django.db import models
from datetime import datetime
from django.utils import timezone

class Jadwal(models.Model):
        
    event = models.CharField(max_length=500, help_text='Event',default = "Class")
    date = models.DateField(default=timezone.now)
    category = models.CharField(max_length=50, help_text='Category',default = "Study")
    location = models.CharField(max_length=50, help_text='Location',default = "Faculty of Computer Science")
    time = models.TimeField(default=timezone.now)
    class Meta: 
        ordering = ['date']

    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return self.category+" event, "+self.event+" on "+self.date.strftime("%m/%d/%Y %H")+" at "+self.location